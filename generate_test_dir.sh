#!/bin/sh

[ -d "test" ] && rm -rf "test"
for i in {000..999}; do
  mkdir -p "test/$i"
  touch "test/$i/$i"
done
