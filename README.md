# RFM: Rust file manager

Minimalistic terminal file manager.

I felt that ranger wasn't fast enought, given the easy tasks that I does
on it. Also I don't even use a half of the features that it provides,
so I decided to build my own.

So, since I also wanted to learn rust, I'm using this to fill both needs.

I'm aiming this to be quite simple, move around directories, copy/move/remove
files, launch a shell on current dir, edit files and no much more. So I'm not
that far of finishing it but I could improve things later.

