# TODO
- ~~Long filenames cause _overflow_ on the terminal
Need a way to cap the filename.~~
- Draw errors somehow.
I dont like the idea of carrying a Result<Dlist<File>> everywhere
but maybe it's the only solution.
- ~~Mark files.~~
- ~~Remove files.~~
- ~~Copy/Move files.~~
- ~~Launch a shell on cwd.~~
- Make change_state to return something to inform run if it needs to redraw.
Maybe it's not a good thing, who knows
- ~~Implement Iterator for Dlist~~ Actually was more difficult than I thought
- ~~Bug: select_bottom on src directory panics with "attempt to substract with overflow"~~
- ~~Idea: use an enum to mark the files.
For now it would be: Selected, Yanked.
Then I would change YankType to be another Enum just with Move and Copy~~
I need to carry the files even when I leave the directory, if I'm going to
do this, I need App to carry the files, which is what is yank field doing.
- ~~Change YankType to use Vec<String> instead of Vec<File>. Might be cheaper.
Also add a None constructor instead of using Option, is that good?~~
- Distinguish between Selected files and Yanked files
- Idea: try to use the usize methods to make more clear the arithmetic behind
the select_* methods
- ~~Marked files are forgotten when change directory~~
- I'm rewrite the message writed by `run_external_cmd`, should I `draw` in `run_external_cmd`?
## ~~**FIND files**~~ MODES!
1. ~~External commands executes when the next key is pressed instead when the key in Normal~~
mode is pressed. Actualy almost every mode change happen in that way.
2. ~~Proper way to quit the program~~
3. ~~Actualy implement Find mode~~

- ~~If I find a way to not only loop with new keys but with also a condition, for example,
I think I could solve 1 and 2 easily.~~

- ~~Find doens't find the file if it is above selected~~

- Design: I couldn't implement something like find_next, be cause I would need
the fidn query outside Find mode and I don't have that now. I don't know if it's
something I want to do, it's not that crucial to have that feature, probably find
would be to reach a file that I'm seeing instead of searching for something.

## Keybinds
- ~~separate module actions, where there goes all the funcionts invoked with keybinds,
they should be &mut App -> () (maybe change in the future)~~
