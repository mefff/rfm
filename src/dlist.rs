use std::fmt::Display;

use std::cmp::{max, min};

use termion::{color, cursor};

#[derive(Debug, Clone)]
pub struct Dlist<T> {
    pub list: Vec<T>,
    pub selected: usize,
    pub offset: usize,
    pub geometry: ((u16, u16), (u16, u16)), // ((left_x, top_y), (right_x, bottom_y))
}

impl<T: Display> Dlist<T> {
    pub fn new(list: Vec<T>, lx: u16, ty: u16, rx: u16, by: u16) -> Dlist<T> {
        Dlist {
            list,
            selected: 0,
            offset: 0,
            geometry: ((lx, ty), (rx, by)),
        }
    }

    pub fn draw(&self, main: bool) {
        let ((lx, ty), (_rx, by)) = self.geometry;

        if !self.list.is_empty() {
            for line in ty..=by + 1 {
                let i = self.offset + line as usize - ty as usize;
                if let Some(elem) = self.list.get(i) {
                    if main && i == self.selected {
                        print!(
                            "{}{}{}{}",
                            cursor::Goto(lx, line),
                            color::Bg(color::Red),
                            elem,
                            color::Bg(color::Reset)
                        );
                    } else {
                        print!("{}{}", cursor::Goto(lx, line), elem);
                    }
                } else {
                    break;
                }
            }
        }
    }

    pub fn select_next(&mut self) {
        let ((_lx, ty), (_rx, by)) = self.geometry;
        if !self.list.is_empty() {
            self.selected = min(self.selected + 1, self.list.len() - 1);
            if self.selected as u16 - self.offset as u16 == by - ty + 1 {
                self.offset += 1;
            }
        }
    }

    pub fn select_prev(&mut self) {
        self.selected = max(self.selected, 1) - 1;
        if let Some(offset) = self.offset.checked_sub(1) {
            if self.selected == offset {
                self.offset -= 1
            }
        }
    }

    pub fn select_top(&mut self) {
        self.selected = 0;
        self.offset = 0;
    }

    pub fn select_bottom(&mut self) {
        let ((_, ty), (_, by)) = self.geometry;
        self.selected = self.list.len().saturating_sub(1);
        self.offset = (self.selected + ty as usize).saturating_sub(by as usize);
    }

    pub fn update_geometry(&mut self, lx: u16, ty: u16, rx: u16, by: u16) {
        self.geometry = ((lx, ty), (rx, by));
    }

    pub fn get_selected_mut(&mut self) -> Option<&mut T> {
        self.list.get_mut(self.selected)
    }

    pub fn get_selected(&self) -> Option<&T> {
        self.list.get(self.selected)
    }

    pub fn set_selected(&mut self, x: usize) {
        while x > self.selected {
            self.select_next();
        }
        while x < self.selected {
            self.select_prev();
        }
    }

    pub fn filter_from_by<F>(&self, from: usize, f: F) -> Vec<usize>
    where
        F: Fn(&T) -> bool,
    {
        let mut ret = Vec::new();
        for i in 0..self.list.len() {
            let index = (i + from) % self.list.len();
            if let Some(elem) = self.list.get(index) {
                if f(elem) {
                    ret.push(index);
                }
            }
        }

        ret
    }

    pub fn binary_search_by<'a, F>(&'a self, f: F) -> Result<usize, usize>
    where
        F: FnMut(&'a T) -> std::cmp::Ordering,
    {
        self.list.binary_search_by(f)
    }
}
