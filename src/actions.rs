use std::path::PathBuf;
use std::process::Command;

use crate::app::{App, FindDir, Mode, YankType};
use crate::file::Mark;

pub fn select_next(app: &mut App) -> bool {
    app.files.select_next();

    true
}

pub fn select_prev(app: &mut App) -> bool {
    app.files.select_prev();

    true
}

pub fn select_top(app: &mut App) -> bool {
    app.files.select_top();

    true
}

pub fn select_bottom(app: &mut App) -> bool {
    app.files.select_bottom();

    true
}

fn enter_dir(app: &mut App) -> bool {
    if let Some(file) = app.files.get_selected() {
        if file.is_joinable() {
            app.path.push(&file.name);
            app.load_files();
        }
    }

    true
}

pub fn clear_find(app: &mut App) -> bool {
    app.pattern = None;
    app.message = String::new();

    true
}

pub fn go_to(app: &mut App, path: &str) -> bool {
    let old_path = app.path.clone();
    app.go_to(PathBuf::from(path).canonicalize().unwrap_or(old_path));
    true
}

pub fn go_parent_dir(app: &mut App) -> bool {
    let current_path = app
        .path
        .file_name()
        .map_or_else(String::new, |p| p.to_str().unwrap().to_string());

    app.path.pop();

    app.load_files();
    let parent_index = app
        .files
        .binary_search_by(|f| f.name.cmp(&current_path))
        .unwrap_or_else(|_| 0);
    app.files.set_selected(parent_index);

    true
}

fn yank_files(app: &mut App, yank: YankType) {
    let selected = app.get_marked(Mark::Select);
    if selected.is_empty() {
        let yanked = app.get_marked(Mark::Yank);
        if !yanked.is_empty() {
            for file in yanked {
                // TODO: can I do a function change_file to do this?
                let index = app
                    .files
                    .binary_search_by(|f| f.cmp(&file))
                    .expect("file MUST be there");
                let mut file = app
                    .files
                    .list
                    .get(index)
                    .expect("file MUST be there")
                    .clone();
                file.mark = Mark::None;
                app.files.list[index] = file;
            }
        }
        app.mark_file(Mark::Yank);
    } else {
        for file in &selected {
            eprintln!("{:?}", file.path);
        }
        for file in &selected {
            eprintln!("{:?}", file);
            let index = app
                .files
                .list
                .binary_search(&file)
                .expect("file MUSTT be there");
            let mut file = app
                .files
                .list
                .get(index)
                .expect("file MUST be there")
                .clone();
            file.mark = Mark::Yank;
            app.files.list[index] = file;
        }
    }

    if let Some(_) = app.files.get_selected() {
        app.yank = yank.map(|_| {
            app.get_marked(Mark::Yank)
                .iter()
                .map(|f| f.path_to_string())
                .collect()
        });
    }
}

pub fn mv(app: &mut App) -> bool {
    yank_files(app, YankType::Move(Vec::new()));

    true
}

pub fn ln(app: &mut App) -> bool {
    yank_files(app, YankType::Link(Vec::new()));

    true
}

pub fn cp(app: &mut App) -> bool {
    yank_files(app, YankType::Copy(Vec::new()));

    true
}

pub fn paste_files(app: &mut App) -> bool {
    match &app.yank {
        YankType::None => (),
        YankType::Copy(args) => {
            Command::new("cp")
                .arg("-r")
                .args(args)
                .arg(app.path.to_str().unwrap())
                .output()
                .unwrap();
            app.load_files();
        }
        YankType::Move(args) => {
            Command::new("mv")
                .args(args)
                .arg(app.path.to_str().unwrap())
                .output()
                .unwrap();
            app.load_files();
        }
        YankType::Link(args) => {
            Command::new("ln")
                .arg("-s")
                .args(args)
                .arg(app.path.to_str().unwrap())
                .output()
                .unwrap();
            app.load_files();
        }
    }

    true
}

pub fn remove_files(app: &mut App) -> bool {
    let marked = app.get_marked(Mark::Select);
    let args = if !marked.is_empty() {
        Some(marked.iter().map(|f| f.path_to_string()).collect())
    } else {
        app.files.get_selected().map(|f| vec![f.path_to_string()])
    };

    if let Some(args) = args {
        Command::new("rm").arg("-rf").args(args).output().unwrap();

        app.load_files();
    }

    true
}

pub fn edit_file(app: &mut App) -> bool {
    if let Some(file) = app.files.get_selected().cloned() {
        let filepath = file.path_to_str();
        set_external_cmd(app, &["vim", filepath]);
    } else {
        app.message = String::from("Nothing to edit");
    }

    false
}

pub fn find(app: &mut App) -> bool {
    // TODO: Should the argument of Find be an Option or this is not
    // to bad?
    app.mode = Mode::Find;
    app.pattern = None;

    false
}

pub fn launch_shell(app: &mut App) -> bool {
    set_external_cmd(app, &["bash"]);

    false
}

pub fn toggle_mark_file_go_down(app: &mut App) -> bool {
    if let Some(file) = app.files.get_selected_mut() {
        match file.mark {
            Mark::Select => file.mark = Mark::None,
            _ => file.mark = Mark::Select,
        }
    }
    select_next(app);

    true
}

pub fn exit(app: &mut App) -> bool {
    app.should_exit = true;

    false
}

fn set_external_cmd(app: &mut App, vargs: &[&str]) {
    let mut vargs = vargs.iter().fold(String::new(), |s, a| s + " " + a);
    vargs.remove(0);
    app.mode = Mode::ExternalCmd(vargs);
}

pub fn find_next(app: &mut App) -> bool {
    app.find_file(FindDir::Forward);

    true
}

pub fn enter(app: &mut App) -> bool {
    if let Some(file) = app.files.get_selected() {
        if file.is_joinable() {
            enter_dir(app)
        } else {
            edit_file(app)
        }
    } else {
        true
    }
}

pub fn find_prev(app: &mut App) -> bool {
    app.find_file(FindDir::Backward);

    true
}
