use std::cmp::min;
use std::cmp::Ordering;
use std::fs::{metadata, DirEntry};
use std::io;
use std::path::PathBuf;

use std::fs::File as FsFile;
use std::io::Read;

use std::process::Command;

use termion::{color, style};

#[derive(PartialEq, Eq, Debug, Clone)]
pub enum Mark {
    None,
    Yank,
    Select,
}

#[derive(Debug, Clone)]
pub enum FileType {
    Dir,
    File,
    Symlink,
}

#[derive(Debug, Clone)]
pub struct File {
    pub name: String,
    pub ftype: FileType,
    pub path: PathBuf,
    pub size: u64,
    pub mark: Mark,
    pub max_name_len: u16,
}

impl File {
    pub fn new(file: DirEntry, max_name_len: u16) -> Result<File, io::Error> {
        let filename = match file.file_name().into_string() {
            Ok(string) => string,
            Err(_) => {
                return Err(io::Error::new(
                    io::ErrorKind::Other,
                    "coulnt parse OsString".to_string(),
                ))
            }
        };

        let filetype = file.file_type()?;
        let ftype: FileType;

        if filetype.is_dir() {
            ftype = FileType::Dir;
        } else if filetype.is_symlink() {
            ftype = FileType::Symlink;
        } else {
            ftype = FileType::File;
        }

        let size = file.metadata()?.len();

        Ok(File {
            name: filename,
            ftype,
            path: file.path(),
            size,
            mark: Mark::None,
            max_name_len,
        })
    }

    pub fn is_joinable(&self) -> bool {
        match self.ftype {
            FileType::Dir => true,
            FileType::File => false,
            FileType::Symlink => {
                if let Ok(metadata) = metadata(&self.path) {
                    metadata.is_dir()
                } else {
                    // TODO: should warn about the error, not just simply say that isn't a dir
                    false
                }
            }
        }
    }

    pub fn cmp(&self, other: &File) -> Ordering {
        if self.name > other.name {
            Ordering::Greater
        } else if self.name < other.name {
            Ordering::Less
        } else {
            Ordering::Equal
        }
    }

    pub fn path_to_str(&self) -> &str {
        self.path.to_str().expect("Couldn't parse OsString")
    }

    pub fn path_to_string(&self) -> String {
        self.path_to_str().to_string()
    }

    pub fn file_info(&self) -> String {
        let output = Command::new("file")
            .arg(self.path.to_str().unwrap())
            .output()
            .unwrap()
            .stdout;

        let mut output = String::from_utf8(output).unwrap();
        let mut result = String::new();

        let n = output.len() / (self.max_name_len as usize);
        for _ in 0..=n {
            let max = min(self.max_name_len as usize, output.len());
            let new: String = output.drain(..max).collect();
            result.push_str(&new);
            result.push('\n');
        }

        result
    }

    pub fn read_content_or_file_info(&self) -> io::Result<String> {
        let mut f = FsFile::open(self.path.as_path())?; // assumes that path is valid
        let mut raw_buf = [0; 4096];
        f.read(&mut raw_buf).unwrap();

        match String::from_utf8(raw_buf[..raw_buf.len()].to_vec()) {
            Ok(s) => Ok(s),
            Err(_) => Ok(self.file_info()),
        }
    }
}

impl Ord for File {
    fn cmp(&self, other: &File) -> Ordering {
        self.cmp(other)
    }
}

impl PartialOrd for File {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Eq for File {}

impl PartialEq for File {
    fn eq(&self, other: &File) -> bool {
        *self.path == *other.path
    }
}

impl PartialEq for FileType {
    fn eq(&self, other: &FileType) -> bool {
        match (&self, other) {
            (FileType::Dir, FileType::Dir) => true,
            (FileType::File, FileType::File) => true,
            (FileType::Symlink, FileType::Symlink) => true,
            _ => false,
        }
    }
}

impl std::fmt::Display for File {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let name = &self.name[..min(self.max_name_len as usize, self.name.len())];
        match self.mark {
            Mark::None => match self.ftype {
                FileType::Dir => write!(
                    f,
                    "{}{}{}{}{}",
                    style::Bold,
                    color::Fg(color::Blue),
                    name,
                    color::Fg(color::Reset),
                    style::Reset,
                ),
                FileType::File => write!(f, "{}", name),
                FileType::Symlink => write!(
                    f,
                    "{}{}{}{}{}",
                    style::Bold,
                    color::Fg(color::Green),
                    name,
                    color::Fg(color::Reset),
                    style::Reset,
                ),
            },

            Mark::Select => match self.ftype {
                FileType::Dir => write!(
                    f,
                    "{}{}{}{}{}{}{}",
                    style::Bold,
                    color::Fg(color::Blue),
                    color::Bg(color::Magenta),
                    name,
                    color::Bg(color::Reset),
                    color::Fg(color::Reset),
                    style::Reset,
                ),
                FileType::File => write!(
                    f,
                    "{}{}{}",
                    color::Bg(color::Magenta),
                    name,
                    color::Bg(color::Reset)
                ),
                FileType::Symlink => write!(
                    f,
                    "{}{}{}{}{}{}{}",
                    style::Bold,
                    color::Fg(color::Green),
                    color::Bg(color::Magenta),
                    name,
                    color::Bg(color::Reset),
                    color::Fg(color::Reset),
                    style::Reset,
                ),
            },
            Mark::Yank => match self.ftype {
                FileType::Dir => write!(
                    f,
                    "{}{}{}{}{}{}{}",
                    style::Bold,
                    color::Fg(color::Blue),
                    color::Bg(color::Yellow),
                    name,
                    color::Bg(color::Reset),
                    color::Fg(color::Reset),
                    style::Reset,
                ),
                FileType::File => write!(
                    f,
                    "{}{}{}",
                    color::Bg(color::Green),
                    name,
                    color::Bg(color::Reset)
                ),
                FileType::Symlink => write!(
                    f,
                    "{}{}{}{}{}{}{}",
                    style::Bold,
                    color::Fg(color::Green),
                    color::Bg(color::Yellow),
                    name,
                    color::Bg(color::Reset),
                    color::Fg(color::Reset),
                    style::Reset,
                ),
            },
        }
    }
}
