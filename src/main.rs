mod actions;
mod app;
mod config;
mod dlist;
mod file;

use std::env;
use std::path::PathBuf;

extern crate termion;

use app::App;

fn main() {
    let args: Vec<String> = env::args().collect();
    let path = if let Some(arg) = args.get(1) {
        PathBuf::from(arg)
            .canonicalize()
            .expect("No such file or directory")
    } else {
        PathBuf::from(".")
            .canonicalize()
            .expect("No such file or directory")
    };

    let mut app = App::new(path, config::KEYBINDS, 3);
    app.run();
}
