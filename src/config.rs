use termion::event::Key;

use actions;
use app::Keybind;

pub static KEYBINDS: &[Keybind] = &[
    (Key::Char('/'), actions::find),
    (Key::Char('S'), actions::launch_shell),
    (Key::Char('h'), actions::go_parent_dir),
    (Key::Char('j'), actions::select_next),
    (Key::Char('k'), actions::select_prev),
    (Key::Char('l'), actions::enter),
    (Key::Char('g'), actions::select_top),
    (Key::Char('G'), actions::select_bottom),
    (Key::Char('e'), actions::edit_file),
    (Key::Char('q'), actions::exit),
    (Key::Char('y'), actions::cp),
    (Key::Char('d'), actions::mv),
    (Key::Char('L'), actions::ln),
    (Key::Char('p'), actions::paste_files),
    (Key::Char('D'), actions::remove_files),
    (Key::Char(' '), actions::toggle_mark_file_go_down),
    (Key::Char('n'), actions::find_next),
    (Key::Char('N'), actions::find_prev),
    (Key::Esc, actions::clear_find),
    (Key::Char('H'), |a| actions::go_to(a, "/home/meff")),
    (Key::Char('P'), |a| actions::go_to(a, "/partition")),
    (Key::Char('V'), |a| actions::go_to(a, "/partition/videos")),
    (Key::Char('?'), |a| actions::go_to(a, "/")),
];
