use std::fs::read_dir;
use std::path::PathBuf;
use std::process::Command;

use std::cmp::min;

use std::io::{stdin, stdout, Write};
use termion::event::Key;
use termion::input::TermRead;
use termion::raw::IntoRawMode;
use termion::{clear, cursor, terminal_size};

use dlist::Dlist;

use file::{File, Mark};

pub type Keybind = (Key, fn(&mut App) -> bool);

#[derive(PartialEq, Eq)]
pub enum FindDir {
    None,
    Forward,
    Backward,
}

#[derive(Debug, Clone)]
pub enum Mode {
    Normal,
    Find,
    ExternalCmd(String),
}

impl Mode {
    fn get_value(&self) -> Option<String> {
        match self {
            Mode::Normal => None,
            Mode::Find => None,
            Mode::ExternalCmd(cmd) => Some(cmd.to_string()),
        }
    }

    fn map<F>(self, f: F) -> Mode
    where
        F: Fn(String) -> String,
    {
        match self {
            Mode::Normal => Mode::Normal,
            Mode::Find => Mode::Find,
            Mode::ExternalCmd(cmd) => Mode::ExternalCmd(f(cmd)),
        }
    }
}

#[derive(Debug)]
pub enum YankType {
    None,
    Copy(Vec<String>),
    Move(Vec<String>),
    Link(Vec<String>),
}

impl YankType {
    #[allow(dead_code)]
    pub fn unwrap(&self) -> Vec<String> {
        match self {
            YankType::None => panic!("Unwrap applied to YankType::None"),
            YankType::Copy(y) => y.to_vec(),
            YankType::Move(y) => y.to_vec(),
            YankType::Link(y) => y.to_vec(),
        }
    }
    pub fn map<F>(self, f: F) -> YankType
    where
        F: FnOnce(Vec<String>) -> Vec<String>,
    {
        match self {
            YankType::None => YankType::None,
            YankType::Copy(y) => YankType::Copy(f(y)),
            YankType::Move(y) => YankType::Move(f(y)),
            YankType::Link(y) => YankType::Link(f(y)),
        }
    }
}

pub struct App {
    pub mode: Mode,
    pub path: PathBuf,
    pub files: Dlist<File>,
    pub should_exit: bool,
    pub yank: YankType,
    pub message: String,
    pub pattern: Option<String>,
    pad: u16,
    max_name_len: u16,
    top: String,
    keybinds: &'static [Keybind],
}

impl App {
    pub fn new(path: PathBuf, keybinds: &'static [Keybind], pad: u16) -> App {
        let (x, y) = terminal_size().expect("Couldn't get terminal size");

        let max_name_len = x / 2 - pad;

        let v: Vec<File> = Vec::new();
        let mut app = App {
            mode: Mode::Normal,
            path: path.clone(),
            files: Dlist::new(v, pad, pad, (x - pad) / 2, y - pad),
            should_exit: false,
            yank: YankType::None,
            pad,
            max_name_len,
            top: String::from(path.to_str().unwrap()),
            message: String::new(),
            keybinds,
            pattern: None,
        };

        app.load_files();

        app
    }

    // TODO: Writing for find doens't render the file view
    // subfile list is fine tho
    fn draw(&self) {
        let (x, y) = terminal_size().expect("Couldn't get terminal size");

        if let Some(file) = self.files.get_selected() {
            let subfiles = self.get_subfiles(&file).unwrap_or_else(|_| Vec::new());
            let subfiles = Dlist::new(
                subfiles,
                self.max_name_len + self.pad,
                self.pad,
                x - self.pad,
                y - self.pad,
            );

            self.files.draw(true);
            subfiles.draw(false);
        }

        self.draw_message();
        self.draw_top();
        self.draw_frame();
    }

    fn draw_vertical_line(&self, pos: u16) {
        let (_, y) = terminal_size().expect("Couldn't get terminal size");
        for i in self.pad..=y - self.pad + 1 {
            print!("{}|", cursor::Goto(pos, i));
        }
    }

    fn draw_horizontal_line(&self, pos: u16) {
        let (x, _) = terminal_size().expect("Couldn't get terminal size");
        for i in self.pad..=x - self.pad + 1 {
            print!("{}-", cursor::Goto(i, pos));
        }
    }

    fn draw_frame(&self) {
        let (x, y) = terminal_size().expect("Couldn't get terminal size");
        self.draw_vertical_line(self.pad - 1);
        self.draw_vertical_line(x / 2 - 1);
        self.draw_vertical_line(x - self.pad + 1);
        self.draw_horizontal_line(self.pad - 1);
        self.draw_horizontal_line(y - self.pad + 1);

        print!("{}+", cursor::Goto(self.pad - 1, self.pad - 1));
        print!("{}+", cursor::Goto(x - self.pad + 1, self.pad - 1));
        print!("{}+", cursor::Goto(self.pad - 1, y - self.pad + 1));
        print!("{}+", cursor::Goto(x - self.pad + 1, y - self.pad + 1));
    }

    fn draw_message(&self) {
        let (_, y) = terminal_size().expect("Couln't get terminal size");
        print!("{}{}", cursor::Goto(2, y - 1), self.message);
    }

    fn draw_top(&self) {
        print!("{}{}", cursor::Goto(1, 1), self.top);
    }

    // TODO: cant truncante some content (tags file for ex), is it a unicode thing?
    fn draw_file_content(&self) -> std::io::Result<()> {
        let (x, y) = terminal_size().expect("Couldn't get terminal size");
        let top = self.pad;
        let bottom = y - self.pad;
        let left = self.max_name_len + self.pad;
        let _right = x - self.pad;
        if let Some(file) = &self.files.get_selected() {
            if !file.is_joinable() {
                let buf = &file.read_content_or_file_info()?;

                let mut lines = buf.lines();
                for i in top..=bottom {
                    if let Some(line) = lines.next() {
                        // tabs fuck the display since their width is >1
                        let line = tabs_to_spaces(line, 2);
                        let line = &line[0..min(self.max_name_len as usize, line.len())];
                        print!("{}{}", cursor::Goto(left, i), line);
                    } else {
                        break;
                    }
                }
            }
        }
        Ok(())
    }

    // TODO: solve code duplication (else cases)
    pub fn load_files(&mut self) {
        let files = self
            .get_files_in_dir(&self.path)
            .unwrap_or_else(|_| Vec::new());

        self.change_files(files);
    }

    fn get_files_in_dir(&self, path: &PathBuf) -> std::io::Result<Vec<File>> {
        let mut files = Vec::new();
        let paths = read_dir(path)?;

        for file in paths {
            if let Ok(file) = file {
                files.push(File::new(file, self.max_name_len).expect("couldnt parse OsString"));
            }
        }

        files.sort_by(|a, b| a.cmp(b));

        Ok(files)
    }

    fn get_subfiles(&self, file: &File) -> std::io::Result<Vec<File>> {
        if file.is_joinable() {
            Ok(self.get_files_in_dir(&file.path)?)
        } else {
            Ok(Vec::new())
        }
    }

    fn change_files(&mut self, files: Vec<File>) {
        let (x, y) = terminal_size().expect("Couldn't get terminal size");
        self.files = Dlist::new(
            files,
            self.max_name_len,
            self.pad,
            x - self.pad,
            y - self.pad,
        );
    }

    pub fn go_to(&mut self, path: PathBuf) {
        self.path = path;
        self.load_files();
    }

    pub fn mark_file(&mut self, m: Mark) {
        if let Some(file) = self.files.get_selected_mut() {
            file.mark = m;
        }
    }

    pub fn get_marked(&self, m: Mark) -> Vec<File> {
        self.files
            .list
            .clone()
            .into_iter()
            .filter(|f| f.mark == m)
            .collect()
    }

    fn update_geometry(&mut self) {
        let (x, y) = terminal_size().expect("Couldn't get terminal size");
        self.max_name_len = x / 2 - self.pad;
        self.files
            .update_geometry(self.pad, self.pad, (x - self.pad) / 2, y - self.pad);
    }

    fn do_action(&mut self, ev: Key) -> bool {
        let mut read_stdin = true;
        for (key, fun) in self.keybinds {
            if *key == ev {
                read_stdin = fun(self);
            }
        }

        read_stdin
    }

    fn run_normal(&mut self, ev: Key) -> bool {
        let read_stdin = self.do_action(ev);
        self.update_geometry();

        self.top = String::from(self.path.to_str().expect("couldnt parse OsString"));
        self.draw();
        let _ = self.draw_file_content();

        read_stdin
    }

    pub fn find_file(&mut self, dir: FindDir) {
	if let Some(pattern) = &self.pattern {
            let matches = self
		.files
		.filter_from_by(self.files.selected, |f| f.name.contains(pattern));
            let index = match dir {
		FindDir::None => 0,
		FindDir::Forward => 1,
		FindDir::Backward => matches.len() - 1,
            };
            if let Some(index) = matches.get(index) {
		self.files.set_selected(*index);
            }
	}
    }

    fn pattern_pop(&mut self) {
	let old_pattern = self.pattern.clone();
	if let Some(mut pattern) = old_pattern {
	    pattern.pop();
	    self.pattern = Some(pattern.to_string());
	}
    }

    fn pattern_push(&mut self, c: char) {
	if c != '\0' {
	    let mut prev_pattern = match &self.pattern {
		None => String::new(),
		Some(p) => p.to_string(),
	    };
	    prev_pattern.push(c);
	    self.pattern = Some(prev_pattern);
	}
    }

    fn run_find(&mut self, ev: Key) -> bool {
        match ev {
            Key::Char('\n') => {
                self.mode = Mode::Normal;
            }
	    Key::Esc => {
		self.mode = Mode::Normal;
		self.pattern = None;
		self.message = String::new();
	    }
            c => {
                match c {
                    Key::Backspace => {
                        self.pattern_pop();
                    }
                    Key::Char(c) => {
			self.pattern_push(c);
                    }
                    _ => (),
                }

		self.find_file(FindDir::None);
		self.message = String::from("/") + &self.pattern.as_ref().unwrap_or(&String::new());
            }
        }

	self.draw();

	true
    }

    fn run_external_cmd(&mut self, cmd: String) -> bool {
        let vargs = cmd.split(' ').collect::<Vec<_>>();
        let vargs = vargs.as_slice();
        let status = Command::new(vargs[0])
            .args(&vargs[1..])
            .current_dir(&self.path)
            .status();

        let msg = match status {
            Ok(s) => {
                if !s.success() {
                    format!("Exited unsuccessfuly: {}", s).to_string()
                } else {
                    String::from("")
                }
            }
            Err(e) => format!("Failed to execute cmd: {}", e).to_string(),
        };
        self.mode = Mode::Normal;
        self.message = msg;

        false
    }

    pub fn run(&mut self) {
        let mut stdout = stdout().into_raw_mode().unwrap();

        print!("{}{}", cursor::Hide, clear::All);
        stdout.flush().unwrap();

        let mut c = Key::Char('\0');
        loop {
            let stdin = stdin();
            print!("{}{}", clear::All, cursor::Goto(1, 1));
            let read_stdin = match &self.mode {
                Mode::Normal => {
                    let read_stdin = self.run_normal(c);
                    if self.should_exit {
                        break;
                    }

                    //self.message = String::new();

                    read_stdin
                }
                Mode::Find => {
                    stdout.suspend_raw_mode().unwrap();
                    stdout.flush().unwrap();
		    eprintln!("{:?}", c);
                    let read_stdin = self.run_find(c);
                    stdout.activate_raw_mode().unwrap();

                    //self.message = String::new();

                    read_stdin
                }
                Mode::ExternalCmd(cmd) => {
                    print!("{}{}{}", clear::All, cursor::Goto(1, 1), cursor::Show);
                    stdout.flush().unwrap();
                    stdout.suspend_raw_mode().unwrap();

                    let cmd_str = cmd.to_string().clone();
                    let read_stdin = self.run_external_cmd(cmd_str);

                    stdout.activate_raw_mode().unwrap();
                    print!("{}{}", clear::All, cursor::Hide);

                    read_stdin
                }
            };

            stdout.flush().unwrap();

            if !read_stdin {
                c = Key::Char('\0');
            } else if let Some(new_c) = stdin.keys().next() {
                c = new_c.unwrap();
            } else {
                break;
            }
        }

        print!("{}{}{}", clear::All, cursor::Goto(1, 1), cursor::Show);
    }
}

fn tabs_to_spaces(line: &str, n: u8) -> String {
    let mut s = String::new();

    for c in line.chars() {
        if c == '\t' {
            for _i in 0..n {
                s.push(' ');
            }
        } else {
            s.push(c);
        }
    }

    s
}
